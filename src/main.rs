use std::time::{Duration, Instant};
use std::thread::sleep;
use rand::Rng;

fn main() {
    println!("\n Автор программы: \x1b[95mDoveLike\x1b[0m");
    println!(" Дискорд автора проги: \x1b[95mCan't Let Hamster#0947\x1b[0m");
    println!("\n \n \x1b[94mИнформация о программе: \x1b[0m \n");
    println!(" Эта программа написана на языке программирования \x1b[93mRust\x1b[0m и генерирует случайные номера мобильного оператора \x1b[93mPhoenix\x1b[0m, котороый работает в \x1b[93mДНР\x1b[0m.\n \n");

    loop {
        let num = rand::thread_rng().gen_range(1111111..=9999999);
        
        let _aaa = Instant::now();
        sleep(Duration::new(01, 1));

        println!(" Случайный номер ДНР: \x1b[92m+7949{num}\x1b[0m");
    }
}
